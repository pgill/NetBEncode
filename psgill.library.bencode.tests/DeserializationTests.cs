﻿using NUnit.Framework;
using psgill.library.bencode.tests.Files;
using psgill.library.bencode.Types;
using System;
using System.IO;

namespace psgill.library.bencode.tests
{
	[TestFixture]
	public sealed class DeserializationTests {

		[Test]
		public void ParseValidTorrentFile_ExpectSuccess() {

			IEntity rootEntity = new BEncodeSerializer( new MemoryStream( TestFiles.ubuntu_16_04_1_desktop_amd64_iso ) ).Deserialize();
			Console.WriteLine( "Root Entity Type: {0}", rootEntity == null ? "NULL" : rootEntity.GetType().FullName );
		}

	}
}
