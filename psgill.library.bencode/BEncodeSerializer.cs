﻿using psgill.library.bencode.Types;

using System.IO;
using System.Text;

namespace psgill.library.bencode {

	public sealed class BEncodeSerializer {

		private Stream m_content;

		public BEncodeSerializer( Stream content ) {
			m_content = content;
		}

		public IEntity Deserialize() {
			return FetchEntity();
		}

		private IEntity FetchEntity() {
			IEntity entity = null;
			int firstByte = m_content.ReadByte();
			switch( firstByte ) {
				case 0x64: // d
					entity = FetchDictionary();
					break;
				case 0x69: // i
					entity = FetchInteger();
					break;
				case 0x6c: // l
					entity = FetchList();
					break;
				default:
					if( firstByte > 0x2f && firstByte < 0x3a ) {
						m_content.Seek( -1, SeekOrigin.Current );
						byte[] byteStr = FetchByteString();
						ByteStringEntity bStrEntity = new ByteStringEntity( byteStr );
						entity = bStrEntity;
					} else {
						m_content.Seek( -1, SeekOrigin.Current );
					}
					break;
			}
			return entity;
		}

		private DictionaryEntity FetchDictionary() {
			DictionaryEntity result = new DictionaryEntity();
			while( m_content.ReadByte() != 0x65 ) { // e
				m_content.Seek( -1, SeekOrigin.Current );
				string key = FetchString();
				IEntity value = FetchEntity();
				result.Add( key, value );
			}
			return result;
		}

		private ListEntity FetchList() {
			ListEntity result = new ListEntity();
			while( m_content.ReadByte() != 0x65 ) {
				m_content.Seek(-1, SeekOrigin.Current);
				IEntity member = FetchEntity();
				if( member != null ) {
					result.Add( member );
				}
			}
			return result;
		}

		private IntegerEntity FetchInteger() {

			int firstByte = m_content.ReadByte();
			bool isNegative = true;
			int result = 0;
			if ( firstByte != 0x2d ) {
				isNegative = false;
				result = firstByte - 0x30;
			}

			while( ( firstByte = m_content.ReadByte() ) != 0x65 ) { // e
				result = result * 10 + ( firstByte - 0x30 );
			}

			return new IntegerEntity( isNegative ? -result : result ) ;
		}

		private string FetchString() {
			byte[] rawStr = FetchByteString();
			string result = Encoding.ASCII.GetString( rawStr );
			return result;
		}

		private byte[] FetchByteString() {
			int length = 0;
			int firstByte;
			while( (firstByte = m_content.ReadByte() ) != 0x3A ) { //:
				 length = length * 10 + ( firstByte - 0x30 );
			}
			byte[] content = new byte[ length ];
			m_content.Read( content, 0, content.Length );
			return content;
		}
	}
}
