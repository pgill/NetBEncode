﻿using System;
using System.Text;

namespace psgill.library.bencode.Types {

	internal sealed class ByteStringEntity : IEntity {

		private readonly byte[] m_value;
		private Lazy<string> m_strValue;

		EntityType IEntity.Type { get { return EntityType.ByteString; } }
		public  byte[] Value { get { return m_value; } }
		public string StringValue { get { return m_strValue.Value; } }

		public ByteStringEntity( byte[] value ) {
			m_value = value;
			m_strValue = new Lazy<string>( () => {
				string strValue = null;
				try {
					strValue = Encoding.ASCII.GetString( m_value );
				} catch { }
				return strValue;
			});
		}
	}
}
