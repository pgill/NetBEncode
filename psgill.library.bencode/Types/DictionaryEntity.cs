﻿using System.Collections.Generic;

namespace psgill.library.bencode.Types {

	internal sealed class DictionaryEntity : IEntity {

		private readonly IDictionary<string,IEntity> m_children;

		EntityType IEntity.Type { get { return EntityType.Dictionary; } }
		public IDictionary<string, IEntity> Value { get { return m_children; } } 

		public DictionaryEntity() {
			m_children = new Dictionary<string, IEntity>();
		}

		internal void Add( string key, IEntity entity ) {
			m_children.Add( key, entity );
		}

	}
}
