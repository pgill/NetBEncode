﻿
namespace psgill.library.bencode.Types {
	public enum EntityType {
		ByteString,
		Integer,
		List,
		Dictionary
	}
}
