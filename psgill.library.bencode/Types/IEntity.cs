﻿namespace psgill.library.bencode.Types {
	public interface IEntity {
		EntityType @Type { get; }
	}
}
