﻿namespace psgill.library.bencode.Types {

	public sealed class IntegerEntity : IEntity {

		private readonly int m_value;

		EntityType IEntity.Type { get { return EntityType.Integer; } }
		public int Value { get { return m_value; } }

		public IntegerEntity( int value ) {
			m_value = value;
		}

	}
}
