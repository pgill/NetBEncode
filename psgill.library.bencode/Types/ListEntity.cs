﻿using System.Collections.Generic;

namespace psgill.library.bencode.Types {

	public sealed class ListEntity : IEntity {

		private readonly IList<IEntity> m_members;

		EntityType IEntity.Type { get { return EntityType.List; } }
		public IList<IEntity> Value { get { return m_members; } }

		public ListEntity() {
			m_members = new List<IEntity>();
		}

		internal void Add( IEntity entity ) {
			m_members.Add( entity );
		}

	}
}